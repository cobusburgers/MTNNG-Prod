<?xml version="1.0" encoding="UTF-8"?><policy>
	<description>Process data payloads (operation-data) for credential provisioning</description>
	<rule>
		<description>Is credential provisioning enabled? Do we have the right command?</description>
		<conditions>
			<and>
				<if-global-variable name="credprov.enable" op="not-equal">true</if-global-variable>
			</and>
			<and>
				<if-operation mode="case" op="not-equal">$credprov.data.result-op$</if-operation>
			</and>
		</conditions>
		<actions>
			<!-- This credential provisioning policy only applies when GCV credprov.enable is true -->
			<do-break/>
		</actions>
	</rule>
	<rule>
		<description>Retrieve the association for the new object from the add-association command and signal OK to provision and change passphrase</description>
		<comment xml:space="preserve">Because we need an association we capture the add-association event and not just the status of the add operation. Once we have the association, we can query back to the application and get the userid. To set or change credentials we have to retrieve the userid from the application in the format the credential provisioned application expects it. We also signal OK for passphrase change. We have to do that to avoid attempting to change the passphrase before the credentials have been provisioned.</comment>
		<conditions>
			<and>
				<if-operation op="equal">add-association</if-operation>
				<if-op-property mode="nocase" name="credprov-do-provisioning" op="equal">true</if-op-property>
				<if-local-variable mode="regex" name="credprov.data.qualified-dn" op="equal">..+</if-local-variable>
				<if-local-variable mode="regex" name="credprov.data.ldap-dn" op="equal">..+</if-local-variable>
				<if-local-variable name="credprov.data.password" op="available"/>
			</and>
		</conditions>
		<actions>
			<do-set-local-variable name="credprov.data.association" scope="driver">
				<arg-string>
					<token-xpath expression="self::add-association/text()"/>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="okToProvision" scope="policy">
				<arg-string>
					<token-text xml:space="preserve">true</token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="okToChangePassphrase" scope="driver">
				<arg-string>
					<token-text xml:space="preserve">true</token-text>
				</arg-string>
			</do-set-local-variable>
		</actions>
	</rule>
	<rule>
		<description>Successful modify: signal OK to provision and for passphrase change</description>
		<comment xml:space="preserve">We signal OK for passphrase change. We have to do that to avoid attempting to change the passphrase before the credentials have been provisioned.</comment>
		<conditions>
			<and>
				<if-operation op="equal">status</if-operation>
				<if-op-property mode="nocase" name="credprov-do-provisioning" op="equal">true</if-op-property>
				<if-xpath op="true">self::status[@level='success']</if-xpath>
				<if-local-variable mode="regex" name="credprov.data.qualified-dn" op="equal">..+</if-local-variable>
				<if-local-variable mode="regex" name="credprov.data.ldap-dn" op="equal">..+</if-local-variable>
				<if-local-variable mode="regex" name="credprov.data.association" op="equal">..+</if-local-variable>
				<if-local-variable name="credprov.data.password" op="available"/>
			</and>
		</conditions>
		<actions>
			<do-set-local-variable name="okToProvision" scope="policy">
				<arg-string>
					<token-text xml:space="preserve">true</token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="okToChangePassphrase" scope="driver">
				<arg-string>
					<token-text xml:space="preserve">true</token-text>
				</arg-string>
			</do-set-local-variable>
		</actions>
	</rule>
	<rule>
		<description>Retrieve userid from the application</description>
		<comment xml:space="preserve">Once we have the association, we can query back to the application and get the userid. To set or change credentials we have to retrieve the userid from the application in the format the credential provisioned application expects it.</comment>
		<conditions>
			<and>
				<if-local-variable mode="nocase" name="okToProvision" op="equal">true</if-local-variable>
				<if-op-property mode="nocase" name="credprov-do-provisioning" op="equal">true</if-op-property>
				<if-local-variable mode="regex" name="credprov.data.association" op="equal">.+</if-local-variable>
			</and>
		</conditions>
		<actions>
			<do-set-local-variable name="userid" scope="policy">
				<arg-string>
					<token-src-attr name="~credprov.application.userid.attr~">
						<arg-association>
							<token-local-variable name="credprov.data.association"/>
						</arg-association>
					</token-src-attr>
				</arg-string>
			</do-set-local-variable>
			<do-if>
				<arg-conditions>
					<and>
						<if-local-variable mode="regex" name="userid" op="not-equal">.+</if-local-variable>
					</and>
				</arg-conditions>
				<arg-actions>
					<do-set-local-variable name="okToProvision" scope="policy">
						<arg-string>
							<token-text xml:space="preserve">false</token-text>
						</arg-string>
					</do-set-local-variable>
					<do-set-local-variable name="okToChangePassphrase" scope="policy">
						<arg-string>
							<token-text xml:space="preserve">false</token-text>
						</arg-string>
					</do-set-local-variable>
				</arg-actions>
			</do-if>
		</actions>
	</rule>
	<rule>
		<description>Set SecretStore user authentication credentials</description>
		<conditions>
			<and>
				<if-global-variable mode="nocase" name="credprov.secretstore.enable" op="equal">true</if-global-variable>
				<if-op-property mode="nocase" name="credprov-do-provisioning" op="equal">true</if-op-property>
				<if-local-variable mode="nocase" name="okToProvision" op="equal">true</if-local-variable>
			</and>
		</conditions>
		<actions>
			<do-set-sso-credential app-id="~credprov.application.credential.id~" store-def-dn="\[root]\~idv.credprov.nss.repository~">
				<arg-dn>
					<token-local-variable name="credprov.data.ldap-dn"/>
				</arg-dn>
				<arg-string name="SecretType">
					<token-global-variable name="credprov.secretstore.sharedsecrettype"/>
				</arg-string>
				<arg-string name="SharedSecretType">
					<token-global-variable name="credprov.secretstore.sharedsecrettype"/>
				</arg-string>
				<arg-string name="EnhancedProtectionFlag">
					<token-global-variable name="credprov.secretstore.enhancedprotectionflag"/>
				</arg-string>
				<arg-string name="EnhancedProtectionPassword">
					<token-named-password name="secretstore-enhanced-protection-password"/>
				</arg-string>
				<arg-string name="Username">
					<token-local-variable name="userid"/>
				</arg-string>
				<arg-string name="Password">
					<token-local-variable name="credprov.data.password"/>
				</arg-string>
			</do-set-sso-credential>
		</actions>
	</rule>
	<rule>
		<description>Set SecureLogin user authentication credentials</description>
		<conditions>
			<and>
				<if-global-variable mode="nocase" name="credprov.securelogin.enable" op="equal">true</if-global-variable>
				<if-op-property mode="nocase" name="credprov-do-provisioning" op="equal">true</if-op-property>
				<if-local-variable mode="nocase" name="okToProvision" op="equal">true</if-local-variable>
			</and>
		</conditions>
		<actions>
			<do-set-sso-credential app-id="~credprov.application.credential.id~" store-def-dn="\[root]\~idv.credprov.nsl.repository~">
				<arg-dn>
					<token-local-variable name="credprov.data.ldap-dn"/>
				</arg-dn>
				<arg-string name="Username">
					<token-local-variable name="userid"/>
				</arg-string>
				<arg-string name="Password">
					<token-local-variable name="credprov.data.password"/>
				</arg-string>
			</do-set-sso-credential>
		</actions>
	</rule>
	<rule>
		<description>Deprovision user credentials from SecretStore</description>
		<conditions>
			<and>
				<if-global-variable mode="nocase" name="credprov.secretstore.enable" op="equal">true</if-global-variable>
				<if-op-property mode="nocase" name="credprov-do-deprovisioning" op="equal">true</if-op-property>
			</and>
		</conditions>
		<actions>
			<do-clear-sso-credential app-id="~credprov.application.credential.id~" store-def-dn="\[root]\~idv.credprov.nss.repository~">
				<arg-dn>
					<token-local-variable name="credprov.data.ldap-dn"/>
				</arg-dn>
				<arg-string name="SecretType">
					<token-global-variable name="credprov.secretstore.sharedsecrettype"/>
				</arg-string>
			</do-clear-sso-credential>
		</actions>
	</rule>
	<rule>
		<description>Deprovision user credentials from SecureLogin store</description>
		<conditions>
			<and>
				<if-global-variable mode="nocase" name="credprov.securelogin.enable" op="equal">true</if-global-variable>
				<if-op-property mode="nocase" name="credprov-do-deprovisioning" op="equal">true</if-op-property>
			</and>
		</conditions>
		<actions>
			<do-clear-sso-credential app-id="~credprov.application.credential.id~" store-def-dn="\[root]\~idv.credprov.nsl.repository~">
				<arg-dn>
					<token-local-variable name="credprov.data.ldap-dn"/>
				</arg-dn>
			</do-clear-sso-credential>
		</actions>
	</rule>
	<rule>
		<description>Set SecureLogin user passphrase and answer</description>
		<comment xml:space="preserve">Change the passphrase and reset driver variable okToChangePassphrase to false.</comment>
		<conditions>
			<and>
				<if-op-property mode="nocase" name="credprov-do-change-passphrase" op="equal">true</if-op-property>
				<if-local-variable mode="nocase" name="okToChangePassphrase" op="equal">true</if-local-variable>
				<if-local-variable name="credprov.data.ldap-dn" op="available"/>
				<if-local-variable name="credprov.data.nslpassphrase.question" op="available"/>
				<if-local-variable name="credprov.data.nslpassphrase.answer" op="available"/>
			</and>
		</conditions>
		<actions>
			<do-set-sso-passphrase store-def-dn="\[root]\~idv.credprov.nsl.repository~">
				<arg-dn>
					<token-local-variable name="credprov.data.ldap-dn"/>
				</arg-dn>
				<arg-string>
					<token-global-variable name="credprov.nslpassphrase.question"/>
				</arg-string>
				<arg-string>
					<token-local-variable name="credprov.data.nslpassphrase.answer"/>
				</arg-string>
			</do-set-sso-passphrase>
			<do-set-local-variable name="okToChangePassphrase" scope="driver">
				<arg-string>
					<token-text xml:space="preserve">false</token-text>
				</arg-string>
			</do-set-local-variable>
		</actions>
	</rule>
	<rule>
		<description>Reset all Credential Provisioning driver variables</description>
		<comment xml:space="preserve">We'll reset all driver variables so that we can start over setting them.</comment>
		<conditions>
			<and/>
		</conditions>
		<actions>
			<do-set-local-variable name="credprov.data.qualified-dn" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="credprov.data.ldap-dn" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="credprov.data.association" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="credprov.data.password" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="credprov.data.nslpassphrase.question" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
			<do-set-local-variable name="credprov.data.nslpassphrase.answer" scope="driver">
				<arg-string>
					<token-text xml:space="preserve"> </token-text>
				</arg-string>
			</do-set-local-variable>
		</actions>
	</rule>
</policy>