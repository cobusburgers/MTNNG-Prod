<?xml version="1.0" encoding="UTF-8"?><policy xmlns:jstring="http://www.novell.com/nxsl/java/java.lang.String">
	<description>Find matching object in Active Directory</description>
	<rule>
		<description>remember relative position in hierarchy</description>
		<comment>The default policy assumes that you want to synchronize a subset of the Identity Vault with Active Directory. this rule marks events in the given containers for processing by adding the unmached-src-dn operational property. You can add subtrees in the Identity Vault for inclusion by adding if-src-dn conditionals here. If you are using mirrored placement, the unmatched-src-dn is used later in the placement rule. If you do not use container based scoping, this rule may be modified or removed. If you change this rule, the placement rules must also be changed to reflect your policy.</comment>
		<conditions>
			<or>
				<if-class-name mode="nocase" op="equal">User</if-class-name>
				<if-src-dn op="in-subtree">~idv.dit.data.users~</if-src-dn>
			</or>
		</conditions>
		<actions>
			<do-set-op-property name="unmatched-src-dn">
				<arg-string>
					<token-unmatched-src-dn convert="true"/>
				</arg-string>
			</do-set-op-property>
		</actions>
	</rule>
	<rule>
		<description>veto out-of-scope events</description>
		<comment>When scoping by container, events outside of the Identity Vault containers defined in the above rule will not have a unmatched-src-dn operational property and will be vetoed. If you do not want to use container based scoping, this rule should be modified or removed.</comment>
		<conditions>
			<and>
				<if-op-property name="unmatched-src-dn" op="not-available"/>
			</and>
		</conditions>
		<actions>
			<do-veto/>
		</actions>
	</rule>
	<rule>
		<description>UserAccount entitlement: do not match existing accounts</description>
		<comment xml:space="preserve">When using the User Account entitlement with the Identity Manager user application or Role Based Entitlements, accounts are created and deleted/disabled by granting or revoking the entitlement. Default policy does not match an existing account in Active Directory if the user is not entitled to an account in Active Directory. Modify or remove this rule if you want entitlement policy to apply to matching accounts in Active Directory. Notice that this may result in the Active Directory account being deleted or disabled.</comment>
		<conditions>
			<and>
				<if-class-name mode="nocase" op="equal">User</if-class-name>
				<if-global-variable mode="nocase" name="drv.entitlement.UserAccount" op="equal">true</if-global-variable>
				<if-entitlement name="UserAccount" op="not-available"/>
			</and>
		</conditions>
		<actions>
			<do-break/>
		</actions>
	</rule>
	<rule>
		<description>generate full name if not in Identity Vault</description>
		<comment xml:space="preserve">Full name policy: Generate a Full Name from Given Name + Surname if one does not already exist. The value is set in the Identity Vault and in the current operation to Active Directory. This policy assumes that Full Name synchronization is enabled in the subscriber filter. If you disable Full Name in the subscriber filter you should not use Full Name mapping.</comment>
		<conditions>
			<and>
				<if-class-name mode="case" op="equal">User</if-class-name>
				<if-global-variable mode="case" name="FullNameMap" op="equal">true</if-global-variable>
				<if-attr name="Full Name" op="not-available"/>
				<if-attr name="Given Name" op="available"/>
			</and>
		</conditions>
		<actions>
			<do-set-local-variable name="gen-full-name">
				<arg-string>
					<token-attr name="Given Name"/>
					<token-text xml:space="preserve"> </token-text>
					<token-attr name="Surname"/>
				</arg-string>
			</do-set-local-variable>
			<do-set-src-attr-value name="Full Name">
				<arg-value>
					<token-xpath expression="normalize-space($gen-full-name)"/>
				</arg-value>
			</do-set-src-attr-value>
			<do-set-dest-attr-value name="Full Name">
				<arg-value>
					<token-xpath expression="normalize-space($gen-full-name)"/>
				</arg-value>
			</do-set-dest-attr-value>
		</actions>
	</rule>
	<rule>
		<description>match users based on NT logon name</description>
		<comment xml:space="preserve">Logon name policy: Match object name from the Identity Vault to the NT logon name in Active Directory. Because sAMAccountName (DirXML-ADAliasName) is unique in the domain, objects are matched anywhere in the destination hierarchy, not just the relative position in the hierarchy.</comment>
		<conditions>
			<and>
				<if-class-name mode="case" op="equal">User</if-class-name>
				<if-global-variable mode="case" name="LogonNameMap" op="equal">true</if-global-variable>
			</and>
		</conditions>
		<actions>
			<do-find-matching-object scope="subtree">
				<arg-dn>
					<token-global-variable name="drv.user.container"/>
				</arg-dn>
				<arg-match-attr name="DirXML-ADAliasName">
					<arg-value type="string">
						<token-replace-all regex="^a-zA-Z0-9\x21\x23-\x29\x2d\x2e\x40\x5e-\x60\x7b\x7d\x7e\xc0-\xf6\xf8-\xff\x410-\x44f" replace-with="">
							<token-src-name/>
						</token-replace-all>
					</arg-value>
				</arg-match-attr>
			</do-find-matching-object>
		</actions>
	</rule>
</policy>